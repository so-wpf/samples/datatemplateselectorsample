﻿namespace DataTemplateSelectorSample
{
    using System.Windows;
    using System.Windows.Controls;

    public class ShapeTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SquareTemplate { get; set; }

        public DataTemplate CircleTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            switch (item)
            {
                case Square _:
                    return this.SquareTemplate;
                case Circle _:
                    return this.CircleTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}