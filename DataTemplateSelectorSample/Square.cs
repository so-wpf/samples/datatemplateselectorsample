﻿namespace DataTemplateSelectorSample
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class Square : INotifyPropertyChanged
    {
        public Square(int width)
        {
            this.Width = width;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Width { get; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}