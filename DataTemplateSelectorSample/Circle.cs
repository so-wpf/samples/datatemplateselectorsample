﻿namespace DataTemplateSelectorSample
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class Circle : INotifyPropertyChanged
    {
        public Circle(int radius)
        {
            this.Radius = radius;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Radius { get; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}